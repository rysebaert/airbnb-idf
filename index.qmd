---
format: html
editor: visual
toc: false
---

# Un portail multi-échelle pour la visualisation et la compréhesion de la dynamique Airbnb en Ile-de-France

> Faire défiler ici un carroussel avec un extrait des visualisations proposées par le site Web.

::: columns
::: {.column width="48%"}
![](img/homepage/left.png)
:::

::: {.column width="48%"}
![](img/homepage/right.png)
:::
:::

## Le projet

-   Issu du monde de la recherche
-   Présenter l'origine du projet (travaux menés jusqu'alors) + contexte d'abordabilité du logement tendue.
-   Articulations avec l'ANR Whisdom

## Organisation du site Web

-   Activité : L'évolution de l'activité Airbnb depuis 2014 en terme d'offre (logements ouverts à la réservation)

## Objectifs

-   Visualisations spatio temporelles multi-échelle des dynamiques (offre, demande) des réservations Airbnb en Ile-de-France et pour Paris et son voisinage (Métropole du Grand Paris).

::: callout-note
## Multi-représentation cartographique

Trois types de représentation cartographiques sont systématiquement proposées sur ce site Web :

-   **Découpages territoriaux** : communes pour l'Ile-de-France et IRIS pour la Métropole du Grand Paris, qui correspond au plus petit découpage territorial en France. Ce sont aussi, surtout pour les communes, des mailles territoriales de décision politique utiles pour évaluer et suivre des tendances observées.
-   **Grilles régulières (données carroyées)** : d'une résolution d'un kilomètre pour l'Ile-de-France et de 200 mètres pour la Métropole du Grand Paris. Ces découpages permettent d'appréhender les dynamiques à une granularité géographique plus fine que les découpages territoriaux. L'INSEE fournit par ailleurs des statistiques dans ces découpages de référence. Compte-tenu de la granularité géographique de ces entités géographiques, un grand nombre de carreaux sont définis par l'absence de données (pas d'offre Airbnb les concernant).
-   **Lissages spatiaux** : Dès lors que les données sont géoréférencées, il est possible de s'affranchir de la maille territoriale en interpolant les attributs des données géoréférencéés dans un voisinage géographique donné. La méthode employée, celle des potentiels de Stewart (1942), consiste en tout lieu de l'espace à estimer les attributs des données d'intérêt dans un voisinage géographique donné, suivant une fonction exponentielle inverse de la distance (Giraud and Commenges 2020). Chaque carreau de grille décrit alors les caractéristiques du marché locatif de courte durée dans son propre contexte géographique. Cette méthode permet de résoudre les effets de MAUP (ou de bruit statistique lié à un trop faible nombre d'observations) et d'observer les structures spatiales, tout en faisant varier les modalités d'agrégation.
:::

-   Démarche de **recherche exploratoire** qui vise à montrer le potentiel de telles données pour une observation sur le long terme des dynamiques du marché locatif de courte durée dans un contexte métropolitain. Ceci afin d'alimenter la réflexion sur un possible portage institutionnel d'une telle démarche ultérieurement. Cette intiative est soutenue par une démarche **reproductible** : L'ensemble des analyses réalisées repose sur des scripts R reproductibles. Celles-ci peuvent dès lors être reproduites et mises à jour à partir du moment où l'**utilisateur dispose du droit d'accès aux données**.

## Les données

-   Origine des données : AIRDNA (quelques mots dessus cf texte louis).
-   Couverture temporelle des données : 2014-2022 avec des petites interrogations sur la méthodo de airDNA
-   Granularité géographique : localisation (précision de l'ordre d'une centaine de mètres) de l'offre Airbnb associée à des attributs relatifs à la disponibilité du logement, ses spécificités (identifiant du propriétaire, taille et type de logement) et sa réservation (nombre de jours réservés, revenus générés).
-   Précaution d'usage : pas d'informations sur les algos utilisés pour le scraping et la construction des indicateurs, localisation géographique floue.
