---
format: html
editor: visual
---

# Analyse de l'activité Airbnb

## L'offre Airbnb

Avant même d'enregistrer une quelconque activité, un logement doit être inscrit sur la plateforme Airbnb. Le propriétaire décidera par la suite du calendrier de son logement en l'ouvrant à la réservation ou non. Ces premiers graphiques montrent la temporalité de la création de logements Airbnb en Ile-de-France.

::: callout-note
### Logement créés

On mesure ici la première date d'inscription d'un logement, actif ou non sur la plateforme Airbnb. Cet indicateur permet de déceler différents pics correspondant à des périodes non pas de forte activité mais d'une volonté des propriétaires d'entrer sur le marché du logement à court terme. Ces pics surviennent-ils au même moment selon les départements ? A quelles périodes correspondent-ils ?
:::

### Évolution temporelle

![](img/crea/crea_1.png)

### Distribution spatiale

-   Commencer en 2012 (et non en 2008)
-   Paris, MGP et Reste de l'Ile-de-France
-   Densité plutôt que figurés par points ?

![](img/crea/crea_2.png)

ou bien

![](img/crea/crea_pct copie.png)

## Disponibilité et réservations

Parallèlement à **l'offre** de logements Airbnb qui provient des hôtes, on distingue la **demande**, qui désigne les réservations de la part des visiteurs. Les deux indicateurs principaux sont donc le nombre de nuitées disponibles et le nombre de nuitées réservées. De ces deux indicateurs, il est possible de calculer le taux de fréquentation et le taux d'occupation, détaillés plus bas.

::: callout-note
### Activité du logement ?

Un logement est dit actif lorsqu'il a été classé comme disponible au moins une fois au cours d'une certaine période (mois ou année), sans forcément être réservé.
:::

### Évolution temporelle

![](img/crea/crea_3.png)

### Nuitées disponibles

![](img/crea/dispo_2017.png)

### Nuitées réservées

![](img/crea/resa_2017.png)

## Taux de fréquentation / occupation

::: callout-note
## Définitions : taux de fréquentation vs taux d'occupation

-   Le **Taux de fréquentation** désigne le nombre de nuitées réservées sur l'ensemble des jours d'une année. Les logements avec un taux de fréquentation élevé sont donc tout simplement ceux qui engendrent le plus de réservations. Le taux de fréquentation permet de repérer les communes où les logements Airbnb enregistrent de nombreuses réservations, peu importe le stock d'offre disponible.

-   Le **Taux d'occupation** quant à lui représente le nombre de nuitées réservées rapportées au nombre total de nuitées disponibles (réservées **et** ouvertes à la réservation). A l'inverse du taux de fréquentation, il n'est pas soumis à quelconque saisonnalité et désigne la probabilité pour un propriétaire de recevoir une réservation si ce dernier rend son logement disponible.
:::

![](img/crea/tauxfreq_2017.png) ![](img/crea/tauxocc_2017.png)

!! Nouvel indicateur (?) : À la commune, Part des logements dont le taux de fréquentation/occupation est supérieur à x %
